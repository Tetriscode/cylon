(ns cylon.core-test
  (:require [clojure.test :refer :all]
            [cylon.core :as core]
            [cylon.io :as io]
            [clojure.spec.alpha :as s]
            [cylon.specs :as specs]))

(deftest gen-spec
  (is (s/conform ::specs/record-spec (first (specs/generate-n-records 1)))))

(def csv-file "output.csv")
(def pipe-file "output.pipe")
(def space-file "output.space")

(defn setup-fixture [f]
  (do (cylon.core/handle-generate {:format "csv" :output csv-file}))
  (do (cylon.core/handle-generate {:format "space" :output space-file}))
  (do (cylon.core/handle-generate {:format "pipe" :output pipe-file}))
  (f)
  (clojure.java.io/delete-file csv-file)
  (clojure.java.io/delete-file space-file)
  (clojure.java.io/delete-file pipe-file))

(use-fixtures :once setup-fixture)

(deftest supported-formats
  (testing "Formats"
    (is (#'core/supported-format? "csv"))
    (is (#'core/supported-format? "pipe"))
    (is (#'core/supported-format? "space"))
    (is (not (#'core/supported-format? "test")))))

(deftest file-gen
  (testing "Comma Delimited"
    (do (core/handle-generate {:format "csv" :output csv-file}))
    (let [records (io/input-file->records csv-file "csv")]
      (is (every? true? (map #(s/valid? ::specs/record-spec %) records)))))
  (testing "Space Delimited"
    (do (core/handle-generate {:format "pipe" :output pipe-file}))
    (let [records (io/input-file->records pipe-file "pipe")]
      (is (every? true? (map #(s/valid? ::specs/record-spec %) records)))))
  (testing "Pipe Delimited"
    (do (core/handle-generate {:format "space" :output space-file}))
    (let [records (io/input-file->records space-file "space")]
      (is (every? true? (map #(s/valid? ::specs/record-spec %) records))))))

(deftest ingest-file
  (testing "Comma Delimited"
    (let [records (core/handle-input {:format "csv" :input csv-file} "option1")]
      (is (every? true? (map #(s/valid? ::specs/record-spec %) records)))))
  (testing "Space Delimited"
    (let [records (core/handle-input {:format "space" :input space-file} "option1")]
      (is (every? true? (map #(s/valid? ::specs/record-spec %) records)))))
  (testing "Pipe Delimited"
    (let [records (core/handle-input {:format "pipe" :input pipe-file} "option1")]
      (is (every? true? (map #(s/valid? ::specs/record-spec %) records))))))

(deftest handle-options
  (testing "Incorrect Options"
    (is (= (core/handle-options {:errors nil}) "Invalid arguments"))
    (is (= (core/handle-options {:errors "Test"}) "Error: Test"))))
