(ns cylon.io-test
  (:require [clojure.test :refer :all]
            [clojure.spec.alpha :as s]
            [cylon.specs :as specs]
            [cylon.io :as io]))

(def test-record {:last-name "last-name"
                  :first-name "first-name"
                  :gender "male"
                  :favorite-color "green"
                  :date-of-birth (clj-time.core/date-time 2010 2 2)})

(def test-file "test-records.csv")
(def test-file-pipe "test-records.pipe")
(def test-file-space "test-records.space")

(defn setup-fixture [f]
  (do (cylon.core/handle-generate {:format "csv" :output test-file}))
  (f)
  (clojure.java.io/delete-file test-file)
  (clojure.java.io/delete-file test-file-pipe)
  (clojure.java.io/delete-file test-file-space))

(use-fixtures :once setup-fixture)

(deftest csv
  (let [output (#'io/records->csv-str (list test-record))]
    (is (= "LastName, FirstName, Gender, FavoriteColor, DateOfBirth\nlast-name, first-name, male, green, 2/2/2010"
           output) "Testing CSV Delimited serialization")))

(deftest pipe
  (let [output (#'io/records->pipe-str (list test-record))]
    (is (= "LastName | FirstName | Gender | FavoriteColor | DateOfBirth\nlast-name | first-name | male | green | 2/2/2010"
           output) "Testing Pipe Delimited serialization")))


(deftest space
  (let [output (#'io/records->space-str (list test-record))]
    (is (= "LastName FirstName Gender FavoriteColor DateOfBirth\nlast-name first-name male green 2/2/2010"
           output) "Testing Space Delimited serialization")))


(deftest file-csv-io
  (let [fname "test-records.csv"
        records (specs/generate-n-records 50)
        out (io/records->output-file! fname "csv" records)
        in-records (io/input-file->records fname)]
    (is (nil? out) "File written successfully")
    (is (every? true? (map #(s/valid? ::specs/record-spec %) in-records)))
    (is (every? true? (map (fn [rec in-rec] (= (:last-name rec) (:last-name in-rec))) records in-records)))
    (is (every? true? (map (fn [rec in-rec] (= (:first-name rec) (:first-name in-rec))) records in-records)))
    (is (every? true? (map (fn [rec in-rec] (= (:gender rec) (:gender in-rec))) records in-records)))
    (is (every? true? (map (fn [rec in-rec] (= (:favorite-color rec) (:favorite-color in-rec))) records in-records)))
    (is (every? true? (map (fn [rec in-rec]
                             (let [bool (clj-time.core/equal? (:date-of-birth rec) (:date-of-birth in-rec))]
                               bool))
                           records in-records)))))

(deftest file-pipe-io
  (let [records (specs/generate-n-records 50)
        out (io/records->output-file! test-file-pipe "pipe" records)
        in-records (io/input-file->records test-file-pipe "pipe")]
    (is (nil? out) "File written successfully")
    (is (every? true? (map #(s/valid? ::specs/record-spec %) in-records)))
    (is (every? true? (map (fn [rec in-rec] (= (:last-name rec) (:last-name in-rec))) records in-records)))
    (is (every? true? (map (fn [rec in-rec] (= (:first-name rec) (:first-name in-rec))) records in-records)))
    (is (every? true? (map (fn [rec in-rec] (= (:gender rec) (:gender in-rec))) records in-records)))
    (is (every? true? (map (fn [rec in-rec] (= (:favorite-color rec) (:favorite-color in-rec))) records in-records)))
    (is (every? true? (map (fn [rec in-rec]
                             (let [bool (clj-time.core/equal? (:date-of-birth rec) (:date-of-birth in-rec))]
                               bool))
                           records in-records)))))

(deftest file-space-io
  (let [records (specs/generate-n-records 50)
        out (io/records->output-file! test-file-space "space" records)
        in-records (io/input-file->records test-file-space "space")]
    (is (nil? out) "File written successfully")
    (is (every? true? (map #(s/valid? ::specs/record-spec %) in-records)))
    (is (every? true? (map (fn [rec in-rec] (= (:last-name rec) (:last-name in-rec))) records in-records)))
    (is (every? true? (map (fn [rec in-rec] (= (:first-name rec) (:first-name in-rec))) records in-records)))
    (is (every? true? (map (fn [rec in-rec] (= (:gender rec) (:gender in-rec))) records in-records)))
    (is (every? true? (map (fn [rec in-rec] (= (:favorite-color rec) (:favorite-color in-rec))) records in-records)))
    (is (every? true? (map (fn [rec in-rec]
                             (let [bool (clj-time.core/equal? (:date-of-birth rec) (:date-of-birth in-rec))]
                               bool))
                           records in-records)))))
