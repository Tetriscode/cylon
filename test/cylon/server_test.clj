(ns cylon.server-test
  (:require [clojure.test :refer :all]
            [clojure.spec.alpha :as spec]
            [clojure.data.json :as json]
            [io.pedestal.test :refer :all]
            [io.pedestal.http :as bootstrap]
            [cylon.server :as server]
            [cylon.specs :as specs]
            [cylon.io :as io]
            [camel-snake-kebab.core :refer :all]))

(def test-file "test-records.csv")

(def test-service-def (server/make-service-def test-file "csv"))

(def service
  (::bootstrap/service-fn (bootstrap/create-servlet test-service-def)))

(defn setup-fixture [f]
  (do (cylon.core/handle-generate {:format "csv" :output test-file}))
  (f)
  (clojure.java.io/delete-file test-file))


(use-fixtures :once setup-fixture)

(deftest records-post-test
  (let [j (-> (specs/generate-n-records 1)
              first
              (update :date-of-birth io/date->date-str)
              (json/write-str :key-fn #(-> % name ->PascalCase)))
          resp (response-for service :post "/records" :body j :headers {"Content-Type" "application/json"})]
    (is (-> (:body resp)
            (json/read-str :key-fn keyword)
            :Success))))

(deftest records-gender-test
  (let [resp (response-for service :get "/records/gender")
        resp-maps (json/read-str (:body resp))
        records (->> resp-maps (map server/translate-keys) (map server/translate-dates))]
    (is (every? true? (map #(spec/valid? ::specs/record-spec %) records))))
  (is (=
        (:headers (response-for service :get "/records/gender"))
        {"Content-Type" "application/json"
         "Strict-Transport-Security" "max-age=31536000; includeSubdomains"
         "X-Frame-Options" "DENY"
         "X-Content-Type-Options" "nosniff"
         "X-XSS-Protection" "1; mode=block"
         "X-Download-Options" "noopen"
         "X-Permitted-Cross-Domain-Policies" "none"
         "Content-Security-Policy" "object-src 'none'; script-src 'unsafe-inline' 'unsafe-eval' 'strict-dynamic' https: http:;"})))

(deftest records-lastname-test
  (let [resp (response-for service :get "/records/name")
        resp-maps (json/read-str (:body resp))
        records (->> resp-maps (map server/translate-keys) (map server/translate-dates))]
    (is (every? true? (map #(spec/valid? ::specs/record-spec %) records))))
  (is (=
        (:headers (response-for service :get "/records/name"))
        {"Content-Type" "application/json"
         "Strict-Transport-Security" "max-age=31536000; includeSubdomains"
         "X-Frame-Options" "DENY"
         "X-Content-Type-Options" "nosniff"
         "X-XSS-Protection" "1; mode=block"
         "X-Download-Options" "noopen"
         "X-Permitted-Cross-Domain-Policies" "none"
         "Content-Security-Policy" "object-src 'none'; script-src 'unsafe-inline' 'unsafe-eval' 'strict-dynamic' https: http:;"})))

(deftest records-birthdate-test
  (let [resp (response-for service :get "/records/birthdate")
        resp-maps (json/read-str (:body resp))
        records (->> resp-maps (map server/translate-keys) (map server/translate-dates))]
    (is (every? true? (map #(spec/valid? ::specs/record-spec %) records))))
  (is (=
        (:headers (response-for service :get "/records/birthdate"))
        {"Content-Type" "application/json"
         "Strict-Transport-Security" "max-age=31536000; includeSubdomains"
         "X-Frame-Options" "DENY"
         "X-Content-Type-Options" "nosniff"
         "X-XSS-Protection" "1; mode=block"
         "X-Download-Options" "noopen"
         "X-Permitted-Cross-Domain-Policies" "none"
         "Content-Security-Policy" "object-src 'none'; script-src 'unsafe-inline' 'unsafe-eval' 'strict-dynamic' https: http:;"})))