(ns cylon.io
  (:require [camel-snake-kebab.core :refer :all]
            [clojure.data.csv :refer [read-csv]]
            [clojure.java.io :as io]
            [clojure.string :refer [join, trim, split]]
            [clj-time.core :as time]
            [clj-time.format :as format]))

(def output-order [:last-name :first-name :gender :favorite-color :date-of-birth])
(def date-format (format/formatter "M/d/YYYY"))

(defn date-str->date
  "This function takes a `date-str` of the format M/d/YYYY and
  returns a LocalDate value"
  [date-str]
  (let [d (format/parse date-format date-str)]
    (time/local-date (time/year d) (time/month d) (time/day d))))

(defn date->date-str [date]
  "This function dates a LocalDate as `date` and returns a date
  representation formatted as M/d/YYYY."
  (format/unparse date-format (time/date-time (time/year date) (time/month date) (time/day date))))

(defn- records->output-str
  "This function takes a list of records and outputs them as a
  string representation using the `delim` as the separator between
  values in the record. LastName, FirstName, Gender... as an example
  where ',' would be the delimiter"
  [delim records]
  (let [header (join delim (map #(-> % name ->PascalCase) output-order))
        rows (->> records
                (map #(update % :date-of-birth date->date-str))
                (map vals)
                (map (partial join delim)))
        all (conj rows header)]
    (join "\n" all)))


(defn- read-csv-file
  "This function takes a file path as `f` and returns a list of lists
  represnting the comma separated values on each row."
  [f]
  (let [reader (io/reader f)]
    (read-csv reader)))

(defn- row-data->records
  "This function takes a list of lists containing record values.
  The first list in `row-data` is header for each column as a PascalCase
  string. These strings are converted to kebab-case and used in the map
  representing a record."
  [row-data]
  (let [str-val-records
        (map zipmap
          (->> (first row-data)
               (map #(-> % keyword ->kebab-case))
               repeat)
          (->> (rest row-data)
               (map #(map trim %))))]
    (map #(update % :date-of-birth date-str->date) str-val-records)))

(defn- read-file
  "This function takes a filename `f` and a delimiter, `delim` to know
  how to break up the values. This is meant to be used by the pipe and
  space delimited files because csv files should use the read-csv-file."
  [f delim]
  (with-open [rdr (io/reader f)]
    (map #(split % delim) (reduce conj [] (line-seq rdr)))))

(defn- records->csv-str
  "This function takes list of `records` and returns a comma delimited
   string to be used to write to a file"
  [records]
  (records->output-str #", " records))

(defn- records->space-str
  "This function takes list of `records` and returns a space delimited
   string to be used to write to a file"
  [records]
  (records->output-str #" " records))

(defn- records->pipe-str
  "This function takes list of `records` and returns a pipe delimited
   string to be used to write to a file"
  [records]
  (records->output-str #" | " records))

(defn input-file->records
  "This function is the preferred function for inputting a file, `f`
  and returning a list of records for comma, space, and pipe delimited
   files. `type` is the string identifier as one of pipe,space, or csv.
   The default ingest format is csv."
  ([f]
   (input-file->records f "csv"))
  ([f type]
   (case type
     "pipe" (row-data->records (read-file f #" \| "))
     "space" (row-data->records (read-file f #" "))
     (row-data->records (read-csv-file f)))))


(defn records->output-file!
  "This function is the preferred function for outputting a file, `f`
  and from a list of `records` as comma, space, and pipe delimited
  files. `type` is the string identifier as one of pipe,space, or csv.
  The default output format is csv."
  ([filename records]
   (records->output-file! filename records "csv"))
  ([filename format records]
   (let [output (case format
                  "pipe" (records->pipe-str records)
                  "space" (records->space-str records)
                  (records->csv-str records))]
     (spit filename output))))

(defn sort-by-gender-last-name
  "Sorts a list of `records` by gender then last name ascending"
  [records]
  (sort-by (juxt :gender (fn [v] (.toUpperCase (:last-name v)))) records))

(defn sort-by-birthdate
  "Sorts a list of `records` by date of birth"
  [records]
  (sort-by :date-of-birth records))

(defn sort-by-last-name
  "Sorts a list of `records` in descending order"
  [records]
  (sort-by :last-name #(compare (.toUpperCase %2) (.toUpperCase %1)) records))