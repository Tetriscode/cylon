(ns cylon.specs
  (:require [clojure.spec.alpha :as spec]
            [clojure.spec.gen.alpha :as spec-gen]
            [clj-time.spec :as time]
            [com.gfredericks.test.chuck.generators :as genc]
            [clojure.spec.alpha :as s]
            [clojure.spec.gen.alpha :as gen]))

(spec/def ::last-name string?)
(spec/def ::first-name string?)
(spec/def ::gender #{"male" "female"})
(spec/def ::colors #{"red" "orange" "yellow" "green" "blue" "purple"})
(spec/def ::favorite-color ::colors)

(spec/def ::year (spec/int-in 1900 3000))
(spec/def ::month (spec/int-in 1 12))
(spec/def ::day (spec/int-in 1 28))

(spec/def ::date-of-birth (spec/with-gen ::time/local-date
                            #(gen/fmap (fn [[year month date]] (clj-time.core/local-date year month date))
                                       (gen/tuple (spec/gen ::year) (spec/gen ::month) (spec/gen ::day)))))

(spec/def ::record-spec (spec/keys :req-un [::last-name
                                            ::first-name
                                            ::gender
                                            ::favorite-color
                                            ::date-of-birth]))

(defn generate-n-records [n]
  (spec-gen/sample (spec/gen ::record-spec) n))