(ns cylon.server
  (:require [io.pedestal.http :as http]
            [io.pedestal.http.route :as route]
            [io.pedestal.http.body-params :as body-params]
            [ring.util.response :as ring-resp]
            [cylon.io :as io]
            [clojure.walk :refer :all]
            [camel-snake-kebab.core :refer :all]
            [clojure.data.json :as json]
            [clojure.tools.logging :as log]))

(defn record-post
  [filename format req]
  (let [record (-> (:json-params req))
        r (->> (io/input-file->records filename format)
               (cons record)
               (io/records->output-file! filename format))]
    (ring-resp/response
      (if (nil? r)
        {:success true :error nil}
        {:success false :error (str r)}))))

(defn name-resp
  [filename format _]
  (-> (io/input-file->records filename format)
      io/sort-by-last-name
      (#(map (fn [v] (update v :date-of-birth io/date->date-str)) %))
      ring-resp/response))

(defn birthday-resp
  [filename format _]
  (-> (io/input-file->records filename format)
      io/sort-by-birthdate
      (#(map (fn [v] (update v :date-of-birth io/date->date-str)) %))
      ring-resp/response))

(defn gender-resp
  [filename format _]
  (-> (io/input-file->records filename format)
      io/sort-by-gender-last-name
      (#(map (fn [v] (update v :date-of-birth io/date->date-str)) %))
      ring-resp/response))

; always set response content-type to json
(defn coerce-to
  [response]
  (-> response
      (update :body json/write-str :key-fn #(-> % name ->PascalCase))
      (assoc-in [:headers "Content-Type"] "application/json")))

(extend-type org.joda.time.LocalDate
  clojure.data.json/JSONWriter
  (-write [local-date out]
    (clojure.data.json/-write (str local-date) out)))

(def coerce-body
  {:name  ::coerce-body
   :leave (fn [context]
            (cond-> context
                    (nil? (get-in context [:response :body :headers "Content-Type"]))
                    (update-in [:response] coerce-to)))})

(defn translate-keys
  "This function converts PascalCase string keys to kebab-case keywords"
  [val]
  (let [f (fn [[k v]] [(-> k ->kebab-case keyword) v])]
    (postwalk (fn [x] (if (map? x) (into {} (map f x)) x)) val)))

(defn translate-dates
  [val]
  (if (some? val) (update val :date-of-birth io/date-str->date)))

(def translate-keys-interceptor
  {:name  ::translate-keys
   :enter (fn [context]
            (update-in context [:request :json-params] translate-keys))})

(def translate-date-interceptor
  {:name  ::translate-dates
   :enter (fn [context]
            (update-in context [:request :json-params] translate-dates))})

(def common-interceptors [coerce-body (body-params/body-params) http/html-body
                          translate-keys-interceptor translate-date-interceptor])

(defn make-routes
  "This function binds the `filename` and `format` to the route."
  [filename format]
  #{["/records" :post (conj common-interceptors (partial record-post filename format))
     :route-name :post-records]
    ["/records/gender" :get (conj common-interceptors (partial gender-resp filename format))
     :route-name :get-gender]
    ["/records/birthdate" :get (conj common-interceptors (partial birthday-resp filename format))
     :route-name :get-birthday]
    ["/records/name" :get (conj common-interceptors (partial name-resp filename format))
     :route-name :get-name]})

(defn make-service-def
  "This function builds the pedestal service defintion with the
  proper file as a data source and its format type."
  [filename format]
  {:env                     :prod
   ::http/routes            #(route/expand-routes (make-routes filename format))
   ::http/resource-path     "/public"
   ::http/type              :jetty
   ::http/port              8080
   ::http/container-options {:h2c? true
                             :h2?  false
                             :ssl? false}})

(defn run-server
  "This function starts a pedestal web server with the `filename` as the data source"
  [filename format]
  (log/info "Starting Web Server")
  (let [server (-> (make-service-def filename format)       ;; start with production configuration
                   (merge {::http/join?           false
                           ::http/allowed-origins {:creds true :allowed-origins (constantly true)}
                           ::http/secure-headers  {:content-security-policy-settings {:object-src "'none'"}}})
                   http/default-interceptors
                   http/dev-interceptors
                   http/create-server)]
    (do (http/start server))
    server))

(defn stop-server
  "This function is intended for REPL development starting
  and stopping the web server."
  [server-instance]
  (http/stop server-instance))

