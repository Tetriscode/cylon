(ns cylon.core
  (:require [clojure.tools.cli :refer [parse-opts]]
            [clojure.pprint :refer [pprint,print-table]]
            [clojure.tools.logging :as log]
            [cylon.io :as io]
            [cylon.server :as server]
            [cylon.specs :as specs])
  (:gen-class))

(defn- supported-format?
  "This function takes a string `fmt` and checks for presence in a list
  of valid input and output formats"
  [fmt]
  (contains? #{"pipe" "csv" "space"} fmt))

(def cli-options
  [["-i" "--input INPUT" "Input File"
    :id :input
    :parse-fn #(str %)]
   ["-g" "--generate" "Generate Data"
    :id :generate]
   ["-f" "--format FORMAT" "Format of File"
    :id :format
    :parse-fn #(str %)
    :validate [supported-format? "Must be a pipe, csv, or space type"]
    :default "csv"]
   ["-o" "--output OUTPUT" "Output File"
    :id :output
    :parse-fn #(str %)]
   ["-w" "--web" "Start Web Server"
    :id :web]
   ["-h" "--help"]])



(def prog-options {"option1" io/sort-by-gender-last-name
                   "option2" io/sort-by-birthdate
                   "option3" io/sort-by-last-name})

(defn handle-generate
  "This functions handles generation of sample data. It takes a
  `format` value and an `output` file name"
  [{:keys [format output]}]
  (let [records (specs/generate-n-records 15)]
    (if (and (some? type) (some? output))
      (io/records->output-file! output format records)
      (log/error "Generate requires Type and Output to be set"))))

(defn handle-input
  "This functions handles ingestion of data. It takes a
  string `format` and a file name as `input`"
  [{:keys [format input]} option-arg]
  (if-let [prog-option (get prog-options option-arg)]
    (let [records (case format
                    "pipe" (io/input-file->records input format)
                    "space" (io/input-file->records input format)
                    (io/input-file->records input format))
          result (prog-option records)]
      (print-table (map (fn [r] (update r :date-of-birth io/date->date-str)) result)))
    (log/errorf "%s is not a valid program option." option-arg)))

(defn handle-web-server
  [{:keys [format input]}]
  (server/run-server input format))

(defn handle-options
  [opts]
  (let [options (:options opts)
        arguments (:arguments opts)]
    (if-not (:errors opts)
      (cond (:generate options) (handle-generate options)
            (:web options) (handle-web-server options)
            (:input options) (handle-input options (first arguments))
            :else "Invalid arguments")
      (str "Error: " (:errors opts)))))

(defn -main
  [& args]
  (log/info (handle-options (parse-opts args cli-options))))
