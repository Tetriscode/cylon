# cylon

## Installation

    lein uberjar

## Test

    lein test

## Usage

    $ java -jar cylon-0.1.0-standalone.jar [opts & args]

## Options

```
-f, --format FORMAT <csv,space,pipe> (default is csv)

-i, --input INPUT (filename of input file)

-o, --output OUTPUT (filename of output file)

-g, --generate (generates sample files as -i and -f)

-w, --web (starts web server)
```

## Examples

#### Generate Files

    $ java -jar cylon-0.1.0-standalone.jar -g -f <space,csv,pipe> -o <outputfilename>

#### Read Files

    $ java -jar cylon-0.1.0-standalone.jar -i <inputfilename> -f <space,csv,pipe> <option1,option2,option3>


#### Run Webserver ####

	$ java -jar cylon-0.1.0-standalone.jar -i <inputfilename> -f <space,csv,pipe> -w

#### Post Record ####

```
curl -X POST \
  http://localhost:8080/records \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/json' \
  -H 'postman-token: be131664-fea3-a72b-c76d-c070e0bea973' \
  -d '{
	"LastName": "Top",
	"FirstName": "Carrot",
	"Gender": "male",
	"FavoriteColor": "purple",
	"DateOfBirth": "1/1/1900"
}'
```